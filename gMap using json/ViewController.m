//
//  ViewController.m
//  gMap using json
//
//  Created by clicklabs124 on 11/3/15.
//  Copyright (c) 2015 rohit. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController () <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet MKMapView *mapViewer;
@property (weak, nonatomic) IBOutlet UIView *googleViewer;
@property (weak, nonatomic) IBOutlet UIImageView *image1;

@end

@implementation ViewController
@synthesize googleViewer;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
    // called when keyboard search button pressed
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
